from django.db import models
from django.conf import settings
from profiles.models import Profile


class UserAccount(models.Model):
    email = models.CharField(max_length=255, null=False)
    username = models.CharField(max_length=20, null=False)
    password = models.CharField(max_length=50)
    ifLogged = models.BooleanField(default=False)
    token = models.CharField(max_length=500, null=True, default="")
    is_email_verified = models.BooleanField(default=False)
    objects = Profile()
    def __str__(self):
        return f"{self.email}{self.username}"
# Create your models here.
