from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
from authservice.models import UserAccount
from profiles.models import Profile

@receiver(post_save, sender=UserAccount)
def save_user_account_profile(sender, instance, created, **kwargs):
    print ("I am here");
    print (sender)
    print (instance)
    print(dir(instance))
    if created:
        Profile.objects.update_or_create(user=instance)