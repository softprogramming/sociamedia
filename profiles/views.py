from django.shortcuts import render
from django.http import  HttpResponse
from profiles.models import  Profile, Relationship
from profiles.forms import ProfileModelForm
from profiles.serializers import UserSerializer
from rest_framework.views import APIView

from django.contrib.auth.models import User
from profiles.models import  Profile

from rest_framework import generics
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from django.views.decorators.csrf import csrf_exempt

def my_profile_view(request):
    print (request.user)
    profile =  Profile.objects.get(user=request.user)
    form = ProfileModelForm(request.POST or None, request.FILES or None,  instance=profile)
    confirm = False

    if request.method == 'POST':
        if form.is_valid():
            form.save();
            confirm = True
    context = {
        'profile':profile,
        'form':form,
         'confirm': confirm,
    }
    print(context)
    # return HttpResponse(request, "hello", )
    return render(request, 'profiles/myprofile.html', context)


def invites_received_view(request):
    profile = Profile.objects.get(user=request.user)
    qs = Relationship.objects.invatations_received(profile)
    results = list(map(lambda x: x.sender, qs))
    is_empty = False
    if len(results) == 0:
        is_empty = True

    context = {
        'qs': results,
        'is_empty': is_empty,
    }

    return render(request, 'profiles/my_invites.html', context)


def profiles_list_view(request):
    user = request.user
    qs = Profile.objects.get_all_profiles(user)

    context = {'qs': qs}

    return render(request, 'profiles/profile_list.html', context)

@csrf_exempt
def sign_up(request):
    print ("I am here in signup")
    print (request.query_params)
    serial = UserSerializer(request.query_params)
    print (serial.email)
    return HttpResponse("hello")

class Record(APIView):
    # get method handler
    def post(self, request):
        # print (request.data)
        serial = UserSerializer(request.data)
        email=request.data.get("email")
        password=request.data.get("password")
        obj = User.objects.create_user(username=email, password=password, email=email)
        print (obj)
        # Profile.objects.get_or_create(user=obj, email=request.data.get("email"), password=request.data.get("password"))
        Profile.objects.create(user=obj)
        return HttpResponse("Hello")

    # queryset = Profile.objects.all()
    # serializer_class = UserSerializer
    # print (serializer_class.email)






