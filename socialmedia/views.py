from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import render


def home_view(request):
    user = request.user
    hello = "hello world"
    context = {
        'user_t': user,
        'hello': hello
    }
    return render(request, 'main/home.html', context)
    # return HttpResponse('Hello World')

# Create your views here.
